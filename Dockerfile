FROM python:3.9-buster
ENV LD_LIBRARY_PATH=/usr/local/lib

RUN apt-get update \
&& apt-get -y install git \
&& apt-get -y install libturbojpeg0 \
&& apt-get -y install libturbojpeg0-dev

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install wheel
RUN python3 -m pip install cffi

RUN git clone -b 1-0-0 https://codebase.helmholtz.cloud/hzb/epics/services/linux-py-mjpeg-proxy.git /camserv/linux-py-mjpeg-proxy
RUN python3 -m pip install -e /camserv/linux-py-mjpeg-proxy

WORKDIR /camserv/linux-py-mjpeg-proxy

CMD ["/usr/local/bin/cameraproxy"]

