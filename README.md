## IP Camera Server

A docker container which hosts a camera server allowing multiple cameras to be viewed from one URL. It acts as a landing page. It also hides complexity required for particular camera models and allows passwords to be specified in one place. 

Modify the cameras.json file to add more cameras. Look in the example file to see how to specify username and password. [For other examples see the source repo.](https://codebase.helmholtz.cloud/hzb/epics/services/linux-py-mjpeg-proxy)

To run, use host networking and make sure to start at boot. You will need to mount your cameras.json file. Note the syntax. You must use an absolute path

```
docker run -it --restart unless-stopped --network host -v /home/emil/Apps/containers/camera_server/camera-server/cameras.json:/camserv/linux-py-mjpeg-proxy/cameras.json:ro registry.hzdr.de/hzb/epics/services/camera-server:latest 
```

To access the camera server go to `localhost:9090`

You will see something like the following page. 

![home_screen](docs/screenshot.png)

Pressing on `Next Image MJPEG` will give you the video stream for that camera. 

[For more detailed instructions see the source code](https://codebase.helmholtz.cloud/hzb/epics/services/linux-py-mjpeg-proxy)

## Rebuilding

To rebuild locally use:

```
docker build -t registry.hzdr.de/hzb/epics/services/camera-server:latest .
```

```
docker push registry.hzdr.de/hzb/epics/services/camera-server:latest
```